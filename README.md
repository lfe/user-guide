# Preface

This Gitbook (available [here](http://lfe.gitbooks.io/user-guide/))
is a work in progress, attempting to synthesize the following elements:

* The best Lisp books
* The topics covered in the best Erlang books
* Elements of the best open source programming language documentation
* Classic language user guides of the 70s, 80s, and 90s.

This is a huge project, and we can use your help! Got an idea? Found a bug?
[Let us know!]().
