A Production of the LFElluminamus

An Imprint of the Alien Alliance 

1 Recursion Way, Stateless, Land of Lisp

First published in your future but copyrighted in your past, &copy; 2014, 2015

The LFE Maintainers assert the moral right to be identified as those who have mostly not completely ignored the scribbles contained herein.

A catalogue for this book is not, to the best of our knowledge, available from the British Library.

![Creative Commons Logo](https://i.creativecommons.org/l/by/4.0/88x31.png)

The LFE User Guide by the LFE Maintainers is licensed under a Creative Commons Attribution 4.0 International License.
Based on a work at http://github.com/lfe/user-guide.