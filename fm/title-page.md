# LFE User Guide

or

## A Guide for Users of the Lisp Flavoured Erlang Language

for

### The Programming of Digital Computing Devices

comprising

#### A Series of Lisp Flavoured Metaprogramming Mystery and Distributed Systems Adventures as they Pertain to the Edification of the Gentle Reader in Her Mastery of Systems of a Timely Sort and Dispositions Quite Tolerant of Faults
